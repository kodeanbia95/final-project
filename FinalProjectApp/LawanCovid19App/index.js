import * as React from 'react';
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import {createBottomTabNavigator} from "@react-navigation/bottom-tabs"
import 'react-native-gesture-handler';

import LoginScreen from './LoginScreen'
import HomeScreen from './HomeScreen'
import ProfileScreen from  './ProfileScreen'
import DetailScreen from './DetailScreen'
import { MaterialCommunityIcons } from '@expo/vector-icons';


const Stack = createStackNavigator()
const bottomTabs = createBottomTabNavigator()
const color = "#045380"

const bottomTabsScreen = () => (
    <bottomTabs.Navigator tabBarOptions={{
        activeTintColor:color
    }}>
        <bottomTabs.Screen 
        name='Home' 
        component={HomeScreen}
        options={{
            tabBarLabel: "Home",
            tabBarIcon:() => (
                <MaterialCommunityIcons name="home" color="#045380" size={40}/>
            )
        }}/>
        <bottomTabs.Screen 
        name='Profile' component={ProfileScreen}
        options={{
            tabBarLabel: "Profile",
            tabBarIcon:() => (
                <MaterialCommunityIcons name="account" color="#045380" size={40}/>
            )
        }}/>
    </bottomTabs.Navigator>
)

export default class Index extends React.Component{
    render(){
        return(
           <NavigationContainer>
               <Stack.Navigator initialRouteName='LoginScreen'>
                   <Stack.Screen name='LoginScreen' component={LoginScreen} options={{headerShown:false}}/>
                   <Stack.Screen name='HomeScreen' component={bottomTabsScreen} options={{headerShown:false}}/>
                   <Stack.Screen name='DetailScreen' component={DetailScreen} options={{headerShown:false}}/>
               </Stack.Navigator>
           </NavigationContainer>
        )
    }
}