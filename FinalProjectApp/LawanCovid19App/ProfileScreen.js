import React from 'react';
import {
    View, 
    Image, 
    StyleSheet,
    Text,
    TouchableOpacity,
    FlatList,
    SafeAreaView,
    TextInput,
    Button,
} from "react-native";
import Icon from 'react-native-vector-icons/MaterialIcons'; 
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";

export default class ProfileScreen extends React.Component {
    constructor(props){
        super(props)
    }
    render(){
        return (
            <View style={{flex:1, backgroundColor:'#003E62', marginTop:20}}>
                <View style={{height:120,backgroundColor:'#00283E'}}>
                    <View style={Styles.containerOne}>
                        <View style={{justifyContent:'center', marginStart:5}}>
                            <Text style={Styles.styleTextOne}>Hari : Rabu</Text>
                            <Text style={Styles.styleTextOne}>Tanggal : 02-03-2020</Text>
                            <Text style={Styles.styleTextOne}>Lokasi : Indonesia</Text>
                        </View>
                        <View style={{justifyContent:'center'}}>
                            <Image 
                            style={{width:140,height:80,}} source={require('./image/logo.png')}></Image>
                        </View>
                    </View>
                    <Text 
                    style={{fontSize:16, color:'white', textAlign:'center', paddingBottom:3,
                    fontWeight:'bold'}}>
                        Kawal informasi seputar COVID19
                        
                        secara tepat dan akurat
                    </Text>
                </View>
                <View style={{height:380, backgroundColor:'#ffffff', 
                    marginStart:15, marginEnd:15, marginTop:30, borderRadius:10,}}>
                    <View style={{height:360, backgroundColor:'#003E62',
                    marginStart:10, marginEnd:10, marginTop:10, borderRadius:10}}>
                        <View style={{flexDirection:'row',justifyContent:"space-between",alignItems:"center"}}>
                            <Image source={{uri:'https://randomuser.me/api/portraits/men/0.jpg'}} 
                            style={{width:70, height:70, borderRadius:35, marginTop:30,marginStart:20}}/>
                            <View 
                            style={{width:130, height:30, backgroundColor:'white',marginTop:30,marginEnd:50,
                            borderRadius:30,justifyContent:"center"}}>
                                <Text 
                                style={{fontSize:14, color:'#045380', fontWeight:'bold',textAlign:'center'
                                }}>
                                    Edit Profil
                                </Text>
                            </View>
                        </View>
                        <View 
                            style={{width:300, height:40, backgroundColor:'white',marginTop:10, marginStart:5,
                            borderRadius:5,justifyContent:"center"}}>
                            <Text 
                                style={{fontSize:14, color:'#045380', fontWeight:'bold',textAlign:'center'
                                }}>
                                    Nama : Ridho Anbia Habibullah
                            </Text>
                        </View>
                        <View 
                            style={{width:300, height:40, backgroundColor:'white',marginTop:10, marginStart:5,
                            borderRadius:5,justifyContent:"center"}}>
                            <Text 
                                style={{fontSize:14, color:'#045380', fontWeight:'bold',textAlign:'center'
                                }}>
                                    email : ridhoanbia09@gmail.com
                            </Text>
                        </View>
                        <View 
                            style={{width:300, height:40, backgroundColor:'white',marginTop:10, marginStart:5,
                            borderRadius:5,justifyContent:"center"}}>
                            <Text 
                                style={{fontSize:14, color:'#045380', fontWeight:'bold',textAlign:'center'
                                }}>
                                    Password : ********
                            </Text>
                        </View>
                        <View 
                            style={{width:300, height:40, backgroundColor:'white',marginTop:10, marginStart:5,
                            borderRadius:5,justifyContent:"center"}}>
                            <Text 
                                style={{fontSize:14, color:'#045380', fontWeight:'bold',textAlign:'center'
                                }}>
                                    Alamat : Jln.Patimura 02, Godong Batu
                            </Text>
                        </View>
                        <View 
                            style={{width:300, height:40, backgroundColor:'white',marginTop:10, marginStart:5,
                            borderRadius:5,justifyContent:"center"}}>
                            <Text 
                                style={{fontSize:14, color:'#045380', fontWeight:'bold',textAlign:'center'
                                }}>
                                    Provinsi : DKI Jakarta

                            </Text>
                        </View>
                    </View>
                </View>
            </View>
        ) 
    }
}

const Styles = StyleSheet.create({
    containerOne:{
        flex:1, flexDirection:'row', justifyContent:'space-between'
    },
    styleTextOne:{
        fontSize:16, color:'white'
    }
})