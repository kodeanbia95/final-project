import React from 'react';
import {
    View, 
    Image, 
    StyleSheet,
    Text,
    TouchableOpacity,
    FlatList,
    SafeAreaView,
    TextInput,
    Button,
} from "react-native";
import Icon from 'react-native-vector-icons/MaterialIcons'; 
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import axios from 'axios'

const api = axios.create({
    baseURL:`https://dekontaminasi.com/api/id/covid19/stats`
}) 

export default class HomeScreen extends React.Component {
    state = {
        arrayData:[],
        terinfeksi:'',
        perawatan:'',
        sembuh:'',
        meninggal:'',
    }
    constructor(props){
        super(props)
        api.get('/').then(res => {
            // console.log(res.data)
            this.setState({arrayData: res.data})
            this.setState({
                terinfeksi:this.state.arrayData.numbers.infected,
                sembuh:this.state.arrayData.numbers.recovered,
                meninggal:this.state.arrayData.numbers.fatal,
            })
        })
    }
    render(){
        return (
            <View style={{flex:1, backgroundColor:'#003E62', marginTop:20}}>
                <View style={{height:120,backgroundColor:'#00283E'}}>
                    <View style={Styles.containerOne}>
                        <View style={{justifyContent:'center', marginStart:5}}>
                            <Text style={Styles.styleTextOne}>Hari : Rabu</Text>
                            <Text style={Styles.styleTextOne}>Tanggal : 02-03-2020</Text>
                            <Text style={Styles.styleTextOne}>Lokasi : Indonesia</Text>
                        </View>
                        <View style={{justifyContent:'center'}}>
                            <Image 
                            style={{width:140,height:80,}} source={require('./image/logo.png')}></Image>
                        </View>
                    </View>
                    <Text 
                    style={{fontSize:16, color:'white', textAlign:'center', paddingBottom:3,
                    fontWeight:'bold'}}>
                        Kawal informasi seputar COVID19
                        secara tepat dan akurat
                    </Text>
                </View>
                <View style={{flex:1}}>
                    <Text 
                    style={{fontSize:16, color:'white', fontWeight:'bold', 
                    marginTop:10, marginStart:5}}>
                        Jumlah Kasus Indonesia saat ini
                    </Text>
                    <View style={{flexDirection:'row',justifyContent:'space-between',
                    marginStart:5,marginEnd:5,marginTop:10}}>
                        <View style={{borderRadius:3, backgroundColor:'white',
                        width:85, height:85, justifyContent:"center"}}>
                            <Text style={{fontWeight:'bold',fontSize:24,textAlign:"center",color:'#ECB448'}}
                            >{this.state.terinfeksi}</Text>
                            <Text style={{fontWeight:'bold',fontSize:12, textAlign:"center"}}
                            >Terkonfirmasi</Text>
                        </View>
                        <View style={{borderRadius:3, backgroundColor:'white',
                        width:85, height:85, justifyContent:"center"}}>
                            <Text style={{fontWeight:'bold',fontSize:24,textAlign:"center",color:'#DC9100'}}
                            >{this.state.terinfeksi - this.state.sembuh - this.state.meninggal}</Text>
                            <Text style={{fontWeight:'bold',fontSize:12, textAlign:"center"}}
                            >Dalam Perawatan</Text>
                        </View>
                        <View style={{borderRadius:3, backgroundColor:'white',
                        width:85, height:85, justifyContent:"center"}}>
                            <Text style={{fontWeight:'bold',fontSize:24,textAlign:"center",color:'#0E7D26'}}
                            >{this.state.sembuh}</Text>
                            <Text style={{fontWeight:'bold',fontSize:12, textAlign:"center"}}
                            >Sembuh</Text>
                        </View>
                        <View style={{borderRadius:3, backgroundColor:'white',
                        width:85, height:85, justifyContent:"center"}}>
                            <Text style={{fontWeight:'bold',fontSize:24,textAlign:"center",color:'#A30909'}}
                            >{this.state.meninggal}</Text>
                            <Text style={{fontWeight:'bold',fontSize:12, textAlign:"center"}}
                            >Meninggal</Text>
                        </View>
                    </View>
                    <Text 
                    style={{fontSize:16, color:'white', fontWeight:'bold', 
                    marginTop:10, marginStart:5}}>
                        Kasus-kasus di Provinsi Indonesia
                    </Text>
                    <View style={{justifyContent:"center",alignSelf:"center"}}>
                        <TouchableOpacity onPress={()=> this.handler()}>
                    <Items/>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        ) 
    }

    handler(){
        this.props.navigation.navigate("DetailScreen")
    }
}

class Items extends React.Component {

    state = {
        arrayData2:[],
        provinsi:'',
        terinfeksi:'',
        sembuh:'',
        meninggal:''
    }
    constructor(props){
        super(props);
        api.get('/').then(res => {
            // console.log(res.data)
            this.setState({arrayData2: res.data.regions})
            this.setState({
                provinsi:this.state.arrayData2[0].name,
                terinfeksi:this.state.arrayData2[0].numbers.infected,
                sembuh:this.state.arrayData2[0].numbers.recovered,
                meninggal:this.state.arrayData2[0].numbers.fatal
            })
        })
    }

    render(){
        return(
            <View style={{
            marginStart:5,marginEnd:5,marginTop:10}}>
                <View style={{borderRadius:3, backgroundColor:'#00283E',
                        width:300, height:200, justifyContent:"center"
                        , borderColor:'white',borderStartWidth:2,borderEndWidth:2,
                        borderTopWidth:2,borderBottomWidth:2}}>
                            <Text style={{fontSize:24, color:'white', fontWeight:'bold',textAlign:'center'}}>
                                Provinsi {this.state.provinsi}
                            </Text>
                            <View style={{flexDirection:"row",justifyContent:"space-evenly"}}>
                                <Text style={{fontWeight:'bold',fontSize:24,textAlign:"center",color:'#ECB448'}}
                                >{this.state.terinfeksi}</Text>
                                <Text 
                                style={{fontWeight:'bold',fontSize:24,textAlign:"center",color:'#A30909'}}
                                >Kasus</Text>
                            </View>
                            
                            <View 
                                style={{width:150,alignSelf:"center",backgroundColor:'white',marginTop:7}}
                                >
                                    <Text style={{fontSize:14, color:'#045380', fontWeight:'bold',textAlign:'center'}}>
                                    Lihat Detail
                                    </Text>
                            </View>
                           
                            
                    </View>
            </View>
        )
    }
}

const Styles = StyleSheet.create({
    containerOne:{
        flex:1, flexDirection:'row', justifyContent:'space-between'
    },
    styleTextOne:{
        fontSize:16, color:'white'
    }
})