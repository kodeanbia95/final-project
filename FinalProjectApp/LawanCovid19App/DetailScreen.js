import React from 'react';
import {
    View, 
    Image, 
    StyleSheet,
    Text,
    TouchableOpacity,
    FlatList,
    SafeAreaView,
    TextInput,
    Button,
} from "react-native";
import Icon from 'react-native-vector-icons/MaterialIcons'; 
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";

export default class HomeScreen extends React.Component {
    constructor(props){
        super(props)
    }
    render(){
        return (
            <View style={{flex:1, backgroundColor:'#003E62', marginTop:20}}>
                <View style={{height:120,backgroundColor:'#00283E'}}>
                    <View style={Styles.containerOne}>
                        <View style={{justifyContent:'center', marginStart:5}}>
                            <Text style={Styles.styleTextOne}>Hari : Rabu</Text>
                            <Text style={Styles.styleTextOne}>Tanggal : {today}</Text>
                            <Text style={Styles.styleTextOne}>Lokasi : Indonesia</Text>
                        </View>
                        <View style={{justifyContent:'center'}}>
                            <Image 
                            style={{width:140,height:80,}} source={require('./image/logo.png')}></Image>
                        </View>
                    </View>
                    <Text 
                    style={{fontSize:16, color:'white', textAlign:'center', paddingBottom:3,
                    fontWeight:'bold'}}>
                        Kawal informasi seputar COVID19
                        secara tepat dan akurat
                    </Text>
                </View>
                <View style={{flex:1, alignItems:"center"}}>
                    <Text 
                    style={{fontSize:24, color:'white', fontWeight:'bold', 
                    marginTop:10, marginStart:5}}>
                        Provinsi Sumatra Barat
                    </Text>
                    <View style={{marginStart:5,marginEnd:5,marginTop:10}}>
                        <View style={{borderRadius:3, backgroundColor:'#00283E',
                        width:200, height:120, justifyContent:"center"
                        , borderColor:'white',borderStartWidth:2,borderEndWidth:2,
                        borderTopWidth:2,borderBottomWidth:2}}>
                            <Text 
                                style={{fontWeight:'bold',fontSize:24,textAlign:"center",color:'#A30909'}}
                                >Kasus Positif</Text>
                            <Text style={{fontWeight:'bold',fontSize:24,textAlign:"center",color:'#ECB448'}}
                                >74018</Text>
                        </View>
                    </View>
                    <View style={{marginStart:5,marginEnd:5,marginTop:10}}>
                        <View style={{borderRadius:3, backgroundColor:'#00283E',
                        width:200, height:120, justifyContent:"center"
                        , borderColor:'white',borderStartWidth:2,borderEndWidth:2,
                        borderTopWidth:2,borderBottomWidth:2}}>
                            <Text 
                                style={{fontWeight:'bold',fontSize:24,textAlign:"center",color:'#0E7D26'}}
                                >Sembuh</Text>
                            <Text style={{fontWeight:'bold',fontSize:24,textAlign:"center",color:'#ECB448'}}
                                >74018</Text>
                        </View>
                    </View>
                    <View style={{marginStart:5,marginEnd:5,marginTop:10}}>
                        <View style={{borderRadius:3, backgroundColor:'#00283E',
                        width:200, height:120, justifyContent:"center"
                        , borderColor:'white',borderStartWidth:2,borderEndWidth:2,
                        borderTopWidth:2,borderBottomWidth:2}}>
                            <Text 
                                style={{fontWeight:'bold',fontSize:24,textAlign:"center",color:'#A30909'}}
                                >Meninggal</Text>
                            <Text style={{fontWeight:'bold',fontSize:24,textAlign:"center",color:'#ECB448'}}
                                >74018</Text>
                        </View>
                    </View>
                </View>
            </View>
        ) 
    }
}

const date=new Date()
const today = `${date.getDate()}/${parseInt(date.getMonth()+1)}/${date.getFullYear()}`

const Styles = StyleSheet.create({
    containerOne:{
        flex:1, flexDirection:'row', justifyContent:'space-between'
    },
    styleTextOne:{
        fontSize:16, color:'white'
    }
})