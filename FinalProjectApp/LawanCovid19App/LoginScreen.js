import React from 'react';
import {
    View, 
    Image, 
    StyleSheet,
    Text,
    TouchableOpacity,
    FlatList,
    SafeAreaView,
    TextInput,
    Button,
} from "react-native";
import Icon from 'react-native-vector-icons/MaterialIcons'; 
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";

export default class LoginScreen extends React.Component {
    constructor(props){
        super(props)
    }
    render(){
        return (
            <View style={{flex:1,backgroundColor:"#0A6294"}}>
                <View style={Styles.container}>
                    <View>
                        <View style={{flexDirection:'row', justifyContent:'space-between'}}>
                            <View style={Styles.containerImage} >
                                <Image source={require('./image/logo.png')}></Image>
                            </View>
                        </View>
                        <View>
                            <Text style={Styles.appTitle}>Selamat Datang</Text>
                        </View>
                        <View>
                            <Text style={Styles.appText}>Masuk untuk melanjutkan</Text>
                        </View>
                        <View style={Styles.inputContainer}>
                            <TextInput 
                                style={Styles.textInput} 
                                placeholder='Email Address'/>
                        </View>
                        <View style={Styles.inputContainer}>
                            <TextInput 
                                style={Styles.textInput} 
                                placeholder='Password'
                                secureTextEntry={true}/>
                        </View>
                        <View>
                            <Text style={Styles.forgotText}>Lupa Password?</Text>
                        </View>
                        <View style={Styles.containerButton} alignSelf="center">
                            <Button
                                title="Masuk"
                                color="#083046"
                                hardwareAccelerated
                                alignSelf="center"
                                onPress = {() => this.props.navigation.navigate("HomeScreen")}
                                />
                        </View> 
                        <View  style={{flexDirection:'row',justifyContent:'center'}}>
                            <Text style={Styles.styleText}>Pengguna baru??</Text>
                            <Text>   </Text>
                            <Text style={Styles.styleText}>Daftar</Text>
                        </View>
                    </View>
                </View>
            </View>
        ) 
    } 
}

const Styles = StyleSheet.create({
    container: {
        flex:1,
        paddingTop:23,
    },
    containerImage: {
        flex:1,
        alignItems:'center',
    },
    appTitle:{
        paddingTop:30,
        paddingLeft:50,
        fontSize:30,
        color:"#ffffff",
        fontWeight: 'bold'
    },
    appText:{
        paddingLeft:50,
        fontSize:18,
        paddingTop:3,
        color:"#ffffff"
    },
    forgotText:{
        paddingLeft:180,
        fontSize:18,
        paddingTop:3,
        color:"#ffffff"
    },
    styleText:{
        fontSize:18,
        paddingTop:30,
        color:"#ffffff"
    },
    styleText2:{
        paddingLeft:20,
        fontSize:18,
        paddingTop:30,
        color:"#ffffff",
        fontWeight:'bold',
        color:"#03324B",
    },
    inputContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        alignSelf: 'center',
        marginBottom: 16
      },
      labelText: {
        fontWeight: 'bold'
      },
      textInput: {
        borderRadius:3,
        marginTop:30,
        height:60,
        width: 300,
        paddingStart:20,
        backgroundColor: 'white',
        fontSize:18
      },
      containerButton:{
          paddingTop:30,
          width:300
      },
      buttonText: {
        fontSize: 24,
        fontWeight: "bold",
        color: "white"
    }
})