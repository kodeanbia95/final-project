import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import LoginScreen from './LawanCovid19App/LoginScreen'
import HomeScreen from './LawanCovid19App/HomeScreen'
import DetailScreen from './LawanCovid19App/DetailScreen'
import Index from './LawanCovid19App/index'

export default function App() {
  return (
    <Index/>
    // <View style={styles.container}>
    //   <Text>Open up App.js to start working on your app!</Text>
    //   <StatusBar style="auto" />
    // </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
